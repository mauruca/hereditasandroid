/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.bluetooth;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 * Created by mauricio on 05/06/16.
 */
public class MensagemBT {

    public static final int aoAtivarBluetooth = 1;
    public static final int aoDesativarBluetooth = 2;
    public static final int aoEncontrarDispositivo = 10;
    public static final int aoPerderDispositivo = 11;
    public static final int aoFinalizarBuscaDispositivo = 12;
    public static final int aoConectar = 20;
    public static final int aoDesconectar = 21;
    public static final int aoFalharConexao = 22;
    public static final int aoEnviarDadosDispositivo = 30;
    public static final int aoLevantarExcecaoTransmitirDados = 31;
    public static final int aoReceberDados = 32;

    private static boolean idValido(int msgId)
    {
        return msgId == aoAtivarBluetooth || msgId == aoDesativarBluetooth ||
                msgId == aoEncontrarDispositivo || msgId == aoPerderDispositivo ||
                msgId == aoFinalizarBuscaDispositivo || msgId == aoConectar ||
                msgId == aoDesconectar || msgId == aoFalharConexao ||
                msgId == aoEnviarDadosDispositivo || msgId == aoLevantarExcecaoTransmitirDados ||
                msgId == aoReceberDados;
    }

    public static void enviaMensagem(Handler h, int msgId)
    {
        if(h == null || !idValido(msgId))
            return;
        h.sendEmptyMessage(msgId);
    }

    public static void enviaMensagem(Handler h, int msgId, String dados)
    {
        if(h == null || !idValido(msgId) || dados == null || dados.trim().length() == 0)
            return;

        Bundle b = new Bundle();
        b.putString("dados",dados);

        Message m = new Message();
        m.what = msgId;
        m.setData(b);
        h.sendMessage(m);
    }
}
