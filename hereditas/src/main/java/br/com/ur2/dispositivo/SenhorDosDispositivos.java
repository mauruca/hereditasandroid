/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.dispositivo;

import br.com.ur2.dispositivo.modelo.Dispositivo;
import br.com.ur2.dispositivo.modelo.DispositivoBluetooth;
import br.com.ur2.dispositivo.rfid.DOTR900;

/**
 * Created by mauricio on 26/05/16.
 */
public class SenhorDosDispositivos {

    public static Dispositivo meEmpresta(Dispositivo.TipoDispositivo tipo)
    {
        if(tipo == null)
            return null;

        switch (tipo)
        {
            case DOTR900:
                return DOTR900.Instance();
            case ZXing:
                return ZXing.Instance();
            default:
                return null;
        }
    }

    public static boolean eBluetooth(Dispositivo dispositivo)
    {
        return dispositivo instanceof DispositivoBluetooth;
    }
}
