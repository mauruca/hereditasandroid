/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.dispositivo;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

import com.google.zxing.integration.android.IntentIntegrator;

import br.com.ur2.dispositivo.modelo.Dispositivo;
import br.com.ur2.dispositivo.modelo.DispositivoBase;
import br.com.ur2.dispositivo.modelo.DispositivoMensagem;
import br.com.ur2.util.uLog;

/**
 * Created by mauricio on 13/06/16.
 */
public class ZXing  extends DispositivoBase implements Dispositivo {

    private static ZXing instance;
    IntentIntegrator integrator;

    public static ZXing Instance() {
        if(instance == null) {
            instance = new ZXing();
        }
        return instance;
    }

    @Override
    public void configuraHandlerDispositivo(Handler handlerDispositivo) {

    }

    @Override
    public void configuraHandlerDados(Handler handlerDados) {
        this.handlerDados = handlerDados;
    }

    @Override
    public Handler obtemHandlerDispositivo() {
        return new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if(msg.what == DispositivoMensagem.aoReceberDados){
                    uLog.Registra("ZXING",DispositivoMensagem.obtemDetalheMensagem(msg));
                    processaPacote(DispositivoMensagem.obtemDetalheMensagem(msg));
                }
            }
        };
    }

    @Override
    public void configuraFormaAtivacao(FormaAtivacao forma) {

    }

    @Override
    public boolean encontrado() {
        return integrator != null;
    }

    @Override
    public boolean ativo() {
        return integrator != null;
    }

    @Override
    public boolean conectado() {
        return integrator != null;
    }

    @Override
    public void ativar(Activity atividade) {

        if(atividade == null)
            return;
        integrator = new IntentIntegrator(atividade);
    }

    @Override
    public void desativar() {
        integrator = null;
    }

    @Override
    public void conectar() {
    }

    @Override
    public void desconectar() {

    }

    @Override
    public void ler(boolean continuous, boolean limpaMemoria) {

        if (limpaMemoria)
        {
            obtemMemoria().limpa();
        }
        integrator.initiateScan();
    }

    @Override
    public void parar() {

    }

    private void processaPacote(String dados)
    {
        obtemMemoria().adicionaEtiqueta(dados);
        DispositivoMensagem.enviaMensagemHandler(handlerDados, DispositivoMensagem.aoReceberDados);
    }

}
