/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.dispositivo.modelo;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 * Created by mauricio on 28/05/16.
 */
public class DispositivoMensagem {

    public static final int aoAtivar = 1;
    public static final int aoDesativar = 2;
    public static final int aoIniciarBuscaDispositivo = 3;
    public static final int aoEncontrarDispositivo = 10;
    public static final int aoPerderDispositivo = 11;
    public static final int aoFinalizarBuscaDispositivo = 12;
    public static final int aoConectar = 20;
    public static final int aoDesconectar = 21;
    public static final int aoFalharConexao = 22;
    public static final int aoEnviarDados = 30;
    public static final int aoLevantarExcecaoTransmitirDados = 31;
    public static final int aoReceberDados = 32;

    private static boolean idValido(int msgId)
    {
        return msgId == aoAtivar || msgId == aoDesativar || msgId == aoIniciarBuscaDispositivo ||
                msgId == aoEncontrarDispositivo || msgId == aoPerderDispositivo ||
                msgId == aoFinalizarBuscaDispositivo || msgId == aoConectar ||
                msgId == aoDesconectar || msgId == aoFalharConexao ||
                msgId == aoEnviarDados || msgId == aoLevantarExcecaoTransmitirDados ||
                msgId == aoReceberDados;
    }

    public static void enviaMensagemHandler(Handler h, int msgId)
    {
        enviaMensagemHandler(h,msgId,null);
    }

    public static void enviaMensagemHandler(Handler h, int msgId, String dados)
    {
        // Handler nulo não levanto erro
        if(h == null)
            return;

        if(!idValido(msgId))
            throw new IllegalArgumentException("Identificador da mensagem é inválido.");

        if( dados == null || dados.trim().length() == 0)
        {
            h.sendEmptyMessage(msgId);
            return;
        }

        Bundle b = new Bundle();
        b.putString("DEMkey" + msgId,dados);

        Message m = new Message();
        m.what = msgId;
        m.setData(b);
        h.sendMessage(m);
    }

    public static String obtemDetalheMensagem(Message msg)
    {
        return msg.getData().getString("DEMkey" + msg.what);
    }
}
