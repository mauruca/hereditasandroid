/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.dispositivo.rfid;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;

import com.dotel.libr900.R900Constants;
import com.dotel.libr900.R900Manager;
import com.dotel.libr900.R900Messages;
import com.dotel.libr900.R900Protocol;
import com.dotel.libr900.R900RecvPacketParser;
import com.dotel.libr900.R900Status;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

import br.com.ur2.dispositivo.modelo.DispositivoBase;
import br.com.ur2.dispositivo.modelo.DispositivoBluetooth;
import br.com.ur2.dispositivo.modelo.DispositivoMensagem;
import br.com.ur2.util.uLog;

/**
 * Created by mauricio on 26/05/16.
 */
public class DOTR900 extends DispositivoBase implements DispositivoBluetooth {

    protected static final String NomeDispositivo = "HQ_UHF_READER";
    protected static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    protected static String TAG = "DOTR900";
    private static DOTR900 instance;
    private final int PRAZO_MS_LEITURA_UNICA = 100;
    final private int tamanhoTag = 24;
    private R900Manager mR900Manager;
    private HashMap<String,BluetoothDevice> dispositivos = new HashMap<>();
    private String ultimoComando = R900Protocol.CMD_INVENT;
    private FormaAtivacao mForma = FormaAtivacao.ATIVAR_SOMENTE;
    private ReentrantLock processando = new ReentrantLock();
    // Mantendo métodos originais do fabricante no controle de memória de tags lidas
    private ArrayList<HashMap<String, String>> mArrTag = new ArrayList<>();
    // IMPLEMENTAÇÃO COM O R900MANAGER MODIFICADO
    // handler assincrono
    private Handler handlerR900Manager = new Handler()
    {
        @Override
        public void handleMessage( final Message msg )
        {
            switch( msg.what )
            {
                case R900Messages.MSG_BT_DATA_RECV:
                    processaPacotes();
                    DispositivoMensagem.enviaMensagemHandler(handlerDados, DispositivoMensagem.aoReceberDados);
                    uLog.Registra(TAG,"MSG_BT_DATA_RECV","Dados recebidos");
                    break;

                case R900Messages.MSG_BT_DATA_SENT:
                    DispositivoMensagem.enviaMensagemHandler(handlerDados, DispositivoMensagem.aoEnviarDados, R900Messages.getByteArrayMessageData(msg).toString());
                    uLog.Registra(TAG,"MSG_BT_DATA_SENT","Dados enviados " + R900Messages.getByteArrayMessageData(msg).toString());
                    break;

                case R900Messages.MSG_BT_DATA_EXCEPTION:
                    DispositivoMensagem.enviaMensagemHandler(handlerDados, DispositivoMensagem.aoLevantarExcecaoTransmitirDados,R900Messages.getStringMessageData(msg));
                    uLog.Registra(TAG,"MSG_BT_DATA_EXCEPTION","Erro >> " + R900Messages.getStringMessageData(msg),2);
                    break;

                case R900Messages.MSG_BT_CONNECTED:
                    // Comando que faz a conexão com o DOT R900
                    sendCmdOpenInterface1();
                    // Atuando conforme a forma de ativação
                    if(mForma.equals(FormaAtivacao.CONECTAR_E_LER))
                        ler(false,true);
                    if(mForma.equals(FormaAtivacao.CONECTAR_E_LER_CONTINUO))
                        ler(true,true);
                    // Aviso a interface que conectei
                    DispositivoMensagem.enviaMensagemHandler(handlerDispositivo, DispositivoMensagem.aoConectar);
                    uLog.Registra(TAG,"MSG_BT_CONNECTED","OK");
                    break;

                case R900Messages.MSG_BT_DISCONNECTED:
                    DispositivoMensagem.enviaMensagemHandler(handlerDispositivo, DispositivoMensagem.aoDesconectar);
                    uLog.Registra(TAG,"MSG_BT_DISCONNECTED","Desconectado");
                    break;

                case R900Messages.MSG_BT_FAILED_CONNECTED:
                    DispositivoMensagem.enviaMensagemHandler(handlerDispositivo, DispositivoMensagem.aoFalharConexao,R900Messages.getStringMessageData(msg));
                    uLog.Registra(TAG,"onBtConnectFail","Conexão falhou >> " + R900Messages.getStringMessageData(msg),2);
                    break;
            }
        }
    };

    public DOTR900()
    {
    }

    public static DOTR900 Instance() {
        if (instance == null) {
            instance = new DOTR900();
            R900Status.setInterfaceMode(R900Constants.MODE_BT_INTERFACE);
            instance.mR900Manager = new R900Manager(instance.handlerR900Manager);
        }
        return instance;
    }

    @Override
    public IntentFilter obtemFiltrosBluetooth() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        return filter;
    }

    @Override
    public BroadcastReceiver obtemEventosBluetooth() {
        return new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent )
            {
                String action = intent.getAction();

                switch (action)
                {
                    case BluetoothDevice.ACTION_FOUND:
                        // Get the BluetoothDevice object from the Intent
                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                        uLog.Registra(TAG, "[Bluetooth] Found Device",NomeDispositivo + "\n"
                                + device.getAddress());

                        // Sendo o evento acionado diretamente pelo getApplicationContext().registerReceiver(
                        if(armazenaDispositivoR900(device))
                            mR900Manager.stopDiscovery();

                        if(!mForma.equals(FormaAtivacao.ATIVAR_SOMENTE))
                            conectar();
                        DispositivoMensagem.enviaMensagemHandler(handlerDispositivo, DispositivoMensagem.aoEncontrarDispositivo);
                        break;

                    case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                        uLog.Registra(TAG,"ACTION_DISCOVERY_FINISHED");
                        DispositivoMensagem.enviaMensagemHandler(handlerDispositivo, DispositivoMensagem.aoFinalizarBuscaDispositivo);
                        break;

                    case BluetoothAdapter.ACTION_STATE_CHANGED:
                        uLog.Registra(TAG,"ACTION_STATE_CHANGED");
                        final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                                BluetoothAdapter.ERROR);
                        switch (state) {
                            case BluetoothAdapter.STATE_OFF:
                                DispositivoMensagem.enviaMensagemHandler(handlerDispositivo, DispositivoMensagem.aoDesativar);
                                break;
                            case BluetoothAdapter.STATE_TURNING_OFF:
                                mR900Manager.disconnect();
                                break;
                            case BluetoothAdapter.STATE_ON:
                                DispositivoMensagem.enviaMensagemHandler(handlerDispositivo, DispositivoMensagem.aoAtivar);
                                // Descobre, conecta e inicia leitura conforme configuração
                                iniciaDescoberta();
                                break;
                            case BluetoothAdapter.STATE_TURNING_ON:
                                // Faz nada
                                break;
                        }
                        break;
                    default:
                        uLog.Registra(TAG, "[Bluetooth] Other event : " + action);
                }
            }
        };
    }

    @Override
    public void configuraHandlerDispositivo(Handler handlerDispositivo) {
        this.handlerDispositivo = handlerDispositivo;
    }

    @Override
    public void configuraHandlerDados(Handler handlerDados) {
        this.handlerDados = handlerDados;
    }

    @Override
    public Handler obtemHandlerDispositivo() {
        return null; // Não utilizado em dispositivo bluetooth
    }

    @Override
    public void configuraFormaAtivacao(FormaAtivacao forma) {
        if(forma == null) {
            mForma = FormaAtivacao.ATIVAR_SOMENTE;
            return;
        }
        mForma = forma;
    }

    @Override
    public boolean encontrado() {
        return mR900Manager.isBluetoothEnabled() && dispositivoEncontrado();
    }

    @Override
    public boolean ativo()
    {
        return encontrado() && conectado();
    }

    @Override
    public boolean conectado() {
        return encontrado() && mR900Manager.isConnected();
    }

    @Override
    public void ativar(Activity atividade)
    {
        if(!mR900Manager.isBluetoothEnabled())
        {
            mR900Manager.enableBluetooth(atividade);
        }
        // Ao ativar o BT é programado para fazer o discovery
        else
            iniciaDescoberta();
    }

    @Override
    public void desativar() {
        desconectar();
        dispositivos.clear();
    }

    @Override
    public void conectar() {
        if(!encontrado())
        {
            DispositivoMensagem.enviaMensagemHandler(handlerDispositivo, DispositivoMensagem.aoFalharConexao,"Dispositivo não encontrado ou bluetooth desativado.");
        }
        if(conectado())
            return;

        mR900Manager.connectToBluetoothDevice(dispositivos.get(NomeDispositivo),MY_UUID);
    }

    @Override
    public void desconectar() {
        parar();
        byeBluetoothDevice();
        if(mR900Manager != null)
            mR900Manager.disconnect();
    }

    @Override
    public void ler(boolean continuous, boolean limpaMemoria) {
        //sendCmdOpenInterface1();
        defineModoReporteInventario();
        if (limpaMemoria)
        {
            obtemMemoria().limpa();
            mArrTag.clear();
        }
        sendCmdInventory(!continuous);
    }

    @Override
    public void parar() {
        sendCmdStop();
    }

    // Metodos privados
    private synchronized void defineModoReporteInventario()
    {
        mR900Manager.sendInventoryReportingFormat(0,1);
    }

    private boolean armazenaDispositivoR900(BluetoothDevice device)
    {
        if(device != null && device.getName() != null && device.getName().equals(NomeDispositivo))
        {
            dispositivos.put(NomeDispositivo,device);
            return true;
        }
        return false;
    }

    private void processaPacotes()
    {
        if(!processando.tryLock())
            return;

        R900RecvPacketParser packetParser = mR900Manager.getRecvPacketParser();
        // ---
        while( true )
        {
            final String parameter = packetParser.popPacket();

            if( parameter != null )
            {
                uLog.Registra(TAG, "processaPacotes","[parameter = " + parameter + "]");
                //processPacket(parameter,false);
                processaPacote(parameter);
            }
            else
                break;
        }
        if(handlerR900Manager != null)
            handlerR900Manager.removeMessages(R900Messages.MSG_BT_DATA_RECV);
        if(handlerDados != null)
            handlerDados.removeMessages(DispositivoMensagem.aoReceberDados);
        processando.unlock();
    }

    private void processaPacote(final String param)
    {
        if( param == null)
            return;

        if (!expectedCommand(param))
        {
            if( ultimoComando == null )
                return;

            if( ultimoComando.equalsIgnoreCase(R900Protocol.CMD_INVENT) )
            {
                // O pacote pode vir com dados de tempo e sinal separados por virgula
                String[] p = param.split(",");

                final float rssi = getRSSI(p);
                final String tagId = p[0].length() > 8 ? p[0].toUpperCase().substring(0, p[0].length() - 4).substring(4): p[0].toUpperCase();
                if(tagId.length() == tamanhoTag)
                    obtemMemoria().adicionaEtiqueta(tagId, rssi);
                else
                    uLog.Registra(TAG,"processaPacote","Tamanho do RFID " + tagId + " >> " + tagId.length(),2);
            }
        }
    }

    private float getRSSI(String[] p)
    {
        if(p == null || p.length == 0 || !p[p.length-1].contains("s=") || p[p.length-1].replaceAll("s=","").trim().length() == 0)
            return 0;
        return Float.parseFloat(p[p.length-1].replaceAll("s=",""));
    }

    private boolean dispositivoEncontrado()
    {
        return dispositivos.size() > 0 && dispositivos.get(NomeDispositivo) != null;
    }

    private void iniciaDescoberta()
    {
        if(!mR900Manager.isBluetoothEnabled())
            return;
        // So entro aqui com bluetooth ativo
        if(!encontrado()) {
            DispositivoMensagem.enviaMensagemHandler(handlerDispositivo, DispositivoMensagem.aoIniciarBuscaDispositivo);
            mR900Manager.startDiscovery();
        }
        else {
            if (mForma == FormaAtivacao.E_CONECTAR || mForma == FormaAtivacao.CONECTAR_E_LER || mForma == FormaAtivacao.CONECTAR_E_LER_CONTINUO)
                conectar();
            if (mForma == FormaAtivacao.CONECTAR_E_LER)
                ler(false,true);
            if(mForma == FormaAtivacao.CONECTAR_E_LER_CONTINUO)
                ler(true,true);
        }
    }

    // Métodos copiados e modificados das classes BluetoothActivity.java e RFIDHostActivity.java
    private void byeBluetoothDevice()
    {
        if( mR900Manager != null )
            mR900Manager.sendData(R900Protocol.BYE);
    }

    private synchronized void sendCmdOpenInterface1()
    {
        if( mR900Manager != null && conectado())
            mR900Manager.sendData(R900Protocol.OPEN_INTERFACE_1);
    }

    private synchronized void sendCmdInventory(boolean mSingleTag)
    {
        sendCmdInventory(mSingleTag, false, false, mSingleTag ? this.PRAZO_MS_LEITURA_UNICA: 0);
    }

    private synchronized void sendCmdInventory(boolean mSingleTag, boolean mUseMask, boolean mQuerySelected, int mTimeout)
    {
        sendCmdInventory( mSingleTag ? 1 : 0, mUseMask ? ( mQuerySelected ? 3 : 2 ) : 0, mSingleTag ? mTimeout : 0);
    }

    private synchronized void sendCmdInventory( int f_s, int f_m, int to )
    {
        if( mR900Manager != null)
        {
            R900Status.setOperationMode(1);	// eric 2012.11.29
            ultimoComando = R900Protocol.CMD_INVENT;
            mR900Manager.sendData(R900Protocol.makeProtocol(ultimoComando, new int[]{ f_s, f_m, to }));
        }
    }

    private synchronized void sendCmdStop()
    {
        if( mR900Manager != null )
        {
            R900Status.setOperationMode(0);	// eric 2012.11.29
            ultimoComando = R900Protocol.CMD_STOP;
            mR900Manager.sendData(R900Protocol.makeProtocol(ultimoComando, (int[])null));
        }
    }

    private boolean expectedCommand(String param)
    {
        final String CMD = param.toLowerCase();

        return (
                CMD.indexOf("^") == 0 || CMD.indexOf("$") == 0 ||
                        CMD.indexOf("ok") == 0 || CMD.indexOf("err") == 0 ||
                        CMD.indexOf("end") == 0 || CMD.indexOf("ireport") == 0);
    }

    private synchronized void processPacket( final String param, boolean skipSame )
    {
        if( param == null || param.length() <= 0 )
            return;

        if (!expectedCommand(param))
        {
            if( ultimoComando == null )
                return;

            if( ultimoComando.equalsIgnoreCase(R900Protocol.CMD_INVENT) )
                updateTagCount(param,skipSame);
            return;
        }

        final String CMD = param.toLowerCase();

        //<-- eric 2013.10.18
        if((CMD.indexOf("$btmac,") == 0))
        {
            final int idxComma = CMD.indexOf(",");
            final String mStrBTmac = CMD.substring(idxComma + 1, CMD.length());

            //mTxtBTmac.setText("bt mac address : " + mStrBTmac);
            return;
        }

        if((CMD.indexOf("ok,ver=") == 0))
        {
            final int idxComma = CMD.indexOf("=");
            final String mStrver = CMD.substring(idxComma + 1, CMD.length());
            //Log.i(TAG, "[[[[[[[[ RECV Monitor ]]]]]]]] idxComma : " + idxComma);
            //Log.i(TAG, "[[[[[[[[ RECV Monitor ]]]]]]]] mStrver : " + mStrver);

            //mTxtFWver.setText("mac." + mStrver);
            return;
        }
        //--> eric 2013.10.18

        //<-- eric 2012.11.23
        // get current battery level
        boolean mBattflag = false;
        if((CMD.indexOf("ok,") != -1) && (!mBattflag))
        {
            final int idxComma = CMD.indexOf(",");
            final String mStrBattLevel = CMD.substring(idxComma + 1, CMD.length());

            /*
            Log.i(TAG, "12312931298389128398129389128391289381298392183918293");
            Log.i(TAG, "[[[[[[[[ RECV Monitor ]]]]]]]] idxComma : " + idxComma);
            Log.i(TAG, "[[[[[[[[ RECV Monitor ]]]]]]]] mStrBattLevel : " + mStrBattLevel);
            Log.i(TAG, "12312931298389128398129389128391289381298392183918293");
            */

            // type(string to int)
            //mBattLevel = Integer.parseInt(mStrBattLevel);
            R900Status.setBatteryLevel(Integer.parseInt(mStrBattLevel));

            // set flag...
            mBattflag = true;
            return;
        }
        //--> eric 2012.11.23


        /*if( CMD.indexOf("$trigger=1") == 0 )
        {
        }
        else if( CMD.indexOf("$trigger=0") == 0 )
        {
        }
        else*/
        if (CMD.indexOf("$online=0") == 0)
        {
            int i = 1;
        }
        else if( CMD.indexOf("end") == 0 )
        {

            final int idxComma = CMD.indexOf(",");
            if( idxComma > 4 )
            {
                final String strNum = CMD.substring(4, idxComma);
                final String strErr = getEndMsg(strNum);
            }

        }
        else if( CMD.indexOf("err") == 0 || CMD.indexOf("ok") == 0 )
        {
            int i = 1;
        }
    }

    private String getEndMsg( String strEnd )
    {
        if( strEnd == null )
            return null;

        if( strEnd.equals("-1") || strEnd.equals("0") )
            return null;// return "Stopped";
        else
            return "Operation Terminated by " + strEnd;
    }

    private void updateTagCount( final String param, boolean mChkSkipSame )
    {
        if( param == null || param.length() <= 4 )
            return;

        //mStrAccessErrMsg = null;
        final String tagId = param.substring(0, param.length() - 4);

        // ---
        boolean bUpdate = false;
        for( HashMap<String, String> map : mArrTag )
        {
            if( tagId.equals(map.get("tag")) )
            {
                if( !mChkSkipSame )
                {
                    final String count = String.valueOf(Integer.parseInt(map
                            .get("count")) + 1);
                    Date curDate = new Date();
                    final String last = curDate.toLocaleString();
                    map.put("count", String.valueOf(count));
                    map.put("last", last);
                    map.put("summary", String.format("- count\t: %s", count));
                }
                bUpdate = true;
                break;
            }
        }

        // ---
        if( bUpdate == false )
        {
            HashMap<String, String> map = new HashMap<String, String>();
            Date curDate = new Date();
            final String count = "1";
            final String first = curDate.toLocaleString();
            map.put("tag", tagId);
            map.put("count", count);
            map.put("first", first);
            map.put("last", first);
            map.put("summary", String.format("- count\t: %s", count));
            mArrTag.add(map);
        }
    }
}
