/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;

import br.com.ur2.dispositivo.modelo.DispositivoMensagem;
import br.com.ur2.hereditas.adaptadores.LocalArrayAdapter;
import br.com.ur2.hereditas.controlador.HereditasControlador;
import br.com.ur2.hereditas.controlador.InventarioDadosControlador;
import br.com.ur2.dispositivo.modelo.Dispositivo;
import br.com.ur2.hereditas.modelo.Local;
import br.com.ur2.hereditas.tarefas.ProcessaLocaisEtiquetasMemoriaAsyncTask;
import br.com.ur2.util.Facilitador;

public class InventarioLocalActivity extends AppCompatActivity {

    ProcessaLocaisEtiquetasMemoriaAsyncTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario_local);

        configurar();

        if(HereditasControlador.getInstance().obtemDispositivo() != null)
        {
            if(HereditasControlador.getInstance().obtemInventariarPorRFID())
                HereditasControlador.getInstance().obtemDispositivo().configuraFormaAtivacao(Dispositivo.FormaAtivacao.CONECTAR_E_LER_CONTINUO);
            else
                HereditasControlador.getInstance().obtemDispositivo().configuraFormaAtivacao(Dispositivo.FormaAtivacao.ATIVAR_SOMENTE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(HereditasControlador.getInstance().obtemInventariarPorRFID())
            HereditasControlador.getInstance().obtemDispositivo().ler(true,true);
        else
            findViewById(R.id.btScanLocalInventario).setVisibility(View.VISIBLE);
        ativar();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(task != null)
            task.cancel(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {
            String re = scanResult.getContents();
            DispositivoMensagem.enviaMensagemHandler(HereditasControlador.getInstance().obtemDispositivo().obtemHandlerDispositivo(),DispositivoMensagem.aoReceberDados,re);
        }
    }

    private void configurar()
    {
        // crio o adaptador para a lista de locais
        final LocalArrayAdapter adapter = new LocalArrayAdapter(this,
                android.R.layout.simple_list_item_1, new ArrayList<Local>());

        ListView lv = (ListView) findViewById(R.id.lvLocal);
        //
        if(lv == null)
            return;

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selecionaItemLista(parent.getAdapter().getItem(position));
            }
        });
    }

    private void ativar()
    {
        HereditasControlador.getInstance().obtemDispositivo().obtemMemoria().limpa();
        InventarioDadosControlador.getInstance().limparMemoriaLocais();
        InventarioDadosControlador.getInstance().limparMemoriaBens();
        HereditasControlador.getInstance().obtemDispositivo().ativar(this);
        task = new ProcessaLocaisEtiquetasMemoriaAsyncTask() {
            @Override
            protected void onProgressUpdate(ArrayList<Local>... values) {
                atualizaListaLocal(values[0]);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void atualizaListaLocal(ArrayList<Local> lista)
    {
        if(lista == null)
            return;

        Object o;
        o = findViewById(R.id.lvLocal);

        if(o == null)
            return;

        ListView lv = (ListView)o;
        ArrayAdapter<Local> ad = ((LocalArrayAdapter)lv.getAdapter());
        if(ad == null)
            return;

        ad.clear();
        ad.addAll(lista);
        ad.notifyDataSetChanged();
    }

    private void selecionaItemLista(Object o)
    {
        task.cancel(true);
        if(o == null)
            return;

        Local l = ((Local)o);

        Facilitador.informa("Selecionado o local " + l.nome,true);
        InventarioDadosControlador.getInstance().localSelecionadoInventario = l;
        Intent intent = new Intent(this, InventarioActivity.class);
        startActivity(intent);
    }

    // Eventos de chamados pela interface
    public void scan(View view)
    {
        atualizaListaLocal(new ArrayList<Local>());
        HereditasControlador.getInstance().obtemDispositivo().obtemMemoria().limpa();
        HereditasControlador.getInstance().obtemDispositivo().ler(true,true);
    }

}
