/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import br.com.ur2.dispositivo.SenhorDosDispositivos;
import br.com.ur2.dispositivo.modelo.Dispositivo;
import br.com.ur2.dispositivo.modelo.DispositivoBluetooth;
import br.com.ur2.dispositivo.modelo.DispositivoMensagem;
import br.com.ur2.hereditas.controlador.AutenticacaoTokenControlador;
import br.com.ur2.hereditas.controlador.BemDadosControlador;
import br.com.ur2.hereditas.controlador.HereditasControlador;
import br.com.ur2.hereditas.controlador.InventarioDadosControlador;
import br.com.ur2.hereditas.controlador.LicencaControlador;
import br.com.ur2.hereditas.controlador.LocalDadosControlador;
import br.com.ur2.hereditas.controlador.PessoaDadosControlador;
import br.com.ur2.hereditas.modelo.Pessoa;
import br.com.ur2.util.Facilitador;
import br.com.ur2.util.uLog;

public class MainActivity extends AppCompatActivity {

    private final int ACCESS_FINE_LOCATION_ID = 7;
    boolean showlogo = false;
    BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HereditasControlador.getInstance().productionServer = true;

        // define variáveis globais do uLog
        uLog.debug = false;
        uLog.nivelRegistro = 3;

        // Configura facilitador do android
        Facilitador.NOME_CONFIGURACAO = "HereditasConfiguracao";
        Facilitador.definirContexto(getBaseContext());

        // Solicita o que for necessário
        verificaPermissoes();

        // Busca dados de configuracoes
        AutenticacaoTokenControlador.getInstance().configurar();
        HereditasControlador.getInstance().configurar();
        LicencaControlador.getInstance().configurar();
        // Verifico a licença na primeira execução
        if(!LicencaControlador.getInstance().licencaVerificada)
        {
            // Chamada assíncrona para verificar a licença
            validarLicenca();
            // espero 1 segundo pelo processamento assincrono,
            // mitigando possibilidade de solicitar configuração antes de validar.
            SystemClock.sleep(1000);
        }

        // Registra responsável por avaliar eventos e filtros de Bluetooth
        if(HereditasControlador.getInstance().obtemDispositivo() != null && SenhorDosDispositivos.eBluetooth(HereditasControlador.getInstance().obtemDispositivo()))
        {
            receiver = ((DispositivoBluetooth) HereditasControlador.getInstance().obtemDispositivo()).obtemEventosBluetooth();
            if(receiver != null)
                getApplicationContext().registerReceiver( receiver,
                        ((DispositivoBluetooth) HereditasControlador.getInstance().obtemDispositivo()).obtemFiltrosBluetooth());

            HereditasControlador.getInstance().obtemDispositivo().configuraHandlerDispositivo(criaHandlerDispositivo());

            HereditasControlador.getInstance().obtemDispositivo().configuraFormaAtivacao(Dispositivo.FormaAtivacao.E_CONECTAR);
            HereditasControlador.getInstance().obtemDispositivo().ativar(this);
        }

        // Apresenta logo
        if(showlogo)
        {
            showlogo = false;
            Intent intent = new Intent(this, LogoActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(receiver != null)
            getApplicationContext().unregisterReceiver(receiver);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(HereditasControlador.getInstance().obtemDispositivo() != null)
        {
            HereditasControlador.getInstance().obtemDispositivo().parar();
        }
        // Test configuration is ok and send to config action
        verificaConfiguracao();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        avaliaRetornoPermissao(requestCode,grantResults);
    }

    // Test configuration
    private void verificaConfiguracao()
    {
        if(HereditasControlador.getInstance().obtemNomeServidor().length() == 0)
        {
            abreConfigActivity(null);
        }
        if(LicencaControlador.getInstance().obtemSerial().length() == 0 || LicencaControlador.getInstance().obtemChave().length() == 0)
        {
            abreConfigActivity(null);
        }
        if(!LicencaControlador.getInstance().licencaVerificada)
        {
            abreConfigActivity(null);
        }
        if(HereditasControlador.getInstance().obtemNomeUsuario().length() == 0 || !AutenticacaoTokenControlador.getInstance().tokenValido())
        {
            abreConfigActivity(null);
        }
    }

    private void validarLicenca()
    {
        LicencaControlador.getInstance().licenca(new ResultadoAssincrono() {
            @Override
            public void AoFinalizarTarefaAssincrona() {

            }

            @Override
            public void AoFinalizarTarefaAssincrona(Boolean bol) {

            }

            @Override
            public void AoFinalizarTarefaAssincrona(String msg) {
                if(msg.length()> 0)
                {
                    Facilitador.informa(Facilitador.obtemString(R.string.licencaFalha) + " " + msg,true);
                    return;
                }
                if(!LicencaControlador.getInstance().licencaVerificada)
                {
                    Facilitador.informa(Facilitador.obtemString(R.string.licencaNExisteInvalida),true);
                }
            }
        });
    }

    // Permissoes
    private void verificaPermissoes()
    {
        if(     (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED))
        {
            ActivityCompat.requestPermissions(this,new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.BLUETOOTH},ACCESS_FINE_LOCATION_ID);
        }
    }

    private void avaliaRetornoPermissao(int requestCode, @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_ID: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length != 3
                        || (
                        grantResults[0] != PackageManager.PERMISSION_GRANTED ||
                                grantResults[1] != PackageManager.PERMISSION_GRANTED ||
                                grantResults[2] != PackageManager.PERMISSION_GRANTED)) {

                    Facilitador.informa("Permissões necessárias não foram obtidas.",false);
                    finish();
                }
            }
        }
    }

    // Intents
    public void abreConfigActivity(View view)
    {
        Intent intent = new Intent(this, ConfigActivity.class);
        startActivity(intent);
    }

    public void abreLocaisActivity(View view)
    {
        Intent intent = new Intent(this, LocaisActivity.class);
        startActivity(intent);
    }

    public void abreBensActivity(View view)
    {
        Intent intent = new Intent(this, BensActivity.class);
        startActivity(intent);
    }

    public void abreInventarioActivity(View view)
    {
        if(!HereditasControlador.getInstance().obtemDispositivo().ativo())
        {
            ativarDispositivo();
            return;
        }

        Pessoa p = PessoaDadosControlador.getInstance().buscaPessoaPorNomeusuario(HereditasControlador.getInstance().obtemNomeUsuario());

        if(LocalDadosControlador.getInstance().contagem() == 0 || BemDadosControlador.getInstance().contagem() == 0 || p == null)
        {
            Facilitador.informa("Não existem dados de inventariante, locais e/ou bens para inicio do inventário.",false);
            return;
        }
        Intent intent = new Intent(this, InventarioLocalActivity.class);
        startActivity(intent);
    }

    // Eventos do dispositivo
    private Handler criaHandlerDispositivo()
    {
        return new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch(msg.what)
                {
                    case DispositivoMensagem.aoAtivar:
                        Facilitador.informa("Bluetooth ligado",true);
                        break;

                    case DispositivoMensagem.aoDesativar:
                        Facilitador.informa("Bluetooth desligado",true);
                        break;

                    case DispositivoMensagem.aoIniciarBuscaDispositivo:
                        Facilitador.informa("Procurando dispositivo...",true);

                    case DispositivoMensagem.aoEncontrarDispositivo:
                        break;

                    case DispositivoMensagem.aoFinalizarBuscaDispositivo:
                        if(!HereditasControlador.getInstance().obtemDispositivo().encontrado())
                            Facilitador.informa("Dispositivo não encontrato.",false);
                        break;

                    case DispositivoMensagem.aoConectar:
                        break;

                    case DispositivoMensagem.aoDesconectar:
                        break;

                    case DispositivoMensagem.aoFalharConexao:
                        Facilitador.informa("Falha ao conectar - " + DispositivoMensagem.obtemDetalheMensagem(msg),false);
                        break;

                    default:
                }
            }
        };
    }

    private void ativarDispositivo()
    {
        Facilitador.informa("Dispositivo inativo. Acionando ativação...",true);
        HereditasControlador.getInstance().obtemDispositivo().ativar(this);
    }

    // Eventos de click no XML
    public void baixarDados(View view)
    {
        if(podeTrocarDadosServidor())
            HereditasControlador.getInstance().baixarDados();
    }

    public void enviarDados(View view) {

        if(!podeTrocarDadosServidor())
            return;

        String msgRetorno = "";

        if(InventarioDadosControlador.getInstance().inventarios.size() == 0)
        {
            msgRetorno = "inventários";
        }
        else
            HereditasControlador.getInstance().enviarDadosInventario();

        if(LocalDadosControlador.getInstance().obtemListaLocaisAtualizar().size() == 0)
        {
            if(msgRetorno.length() > 0) { msgRetorno += ", ";}
            msgRetorno += "locais";
        }
        else
            HereditasControlador.getInstance().enviaDadosLocais();

        if(BemDadosControlador.getInstance().obtemListaBensAtualizar().size() == 0)
        {
            if(msgRetorno.length() > 0) { msgRetorno += ", ";}
            msgRetorno += "bens";
        }
        else
            HereditasControlador.getInstance().enviaDadosBens();

        if(msgRetorno.length() > 0)
            Facilitador.informa("Não existem " + msgRetorno + " a serem enviados.",false);
    }

    private boolean podeTrocarDadosServidor()
    {
        if(!LicencaControlador.getInstance().licencaVerificada)
        {
            Facilitador.informa("Licença não está válida. Veja as configurações.",false);
            return false;
        }
        if(!AutenticacaoTokenControlador.getInstance().tokenValido())
        {
            Facilitador.informa("Usuário não está autenticado no servidor. Veja as configurações.",false);
            return false;
        }
        return true;
    }
}
