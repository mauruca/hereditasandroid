/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas;

import android.graphics.Path;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

import br.com.ur2.util.uLog;

public class PrincipalActivity extends AppCompatActivity {

    GestureDetectorCompat mGesture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        // define variáveis globais do uLog
        uLog.debug = true;
        uLog.nivelRegistro = 3;

        eventos();
    }

    private void eventos()
    {
        if (findViewById(R.id.ibMenu) != null) {

            mGesture = new GestureDetectorCompat(getBaseContext(), new GestureDetector.OnGestureListener() {
                @Override
                public boolean onDown(MotionEvent e) {

                    return false;
                }

                @Override
                public void onShowPress(MotionEvent e) {
                    uLog.Registra("press","Instruções");
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    return false;
                }

                @Override
                public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                    return false;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    uLog.Registra("long","Config");
                }

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

                    float x1 = e1.getX();
                    float y1 = e1.getY();

                    float x2 = e2.getX();
                    float y2 = e2.getY();

                    Direction direction = getDirection(x1,y1,x2,y2);

                    switch (direction.name().toLowerCase())
                    {
                        case "up":
                            trocaCor(R.mipmap.b1);
                            break;
                        case "down":
                            trocaCor(R.mipmap.b2);
                            break;
                        case "left":
                            trocaCor(R.mipmap.b3);
                            break;
                    }

                    uLog.Registra("fly",direction.name());

                    return true;
                }
            });

            ((ImageButton)findViewById(R.id.ibMenu)).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return mGesture.onTouchEvent(event);
                }
            });

        }
    }

    private void trocaCor(int id)
    {
        if (findViewById(R.id.ibMenu) != null) {
            ((ImageButton)findViewById(R.id.ibMenu)).setImageResource(id);
        }
    }

    /**
     * Given two points in the plane p1=(x1, x2) and p2=(y1, y1), this method
     * returns the direction that an arrow pointing from p1 to p2 would have.
     * @param x1 the x position of the first point
     * @param y1 the y position of the first point
     * @param x2 the x position of the second point
     * @param y2 the y position of the second point
     * @return the direction
     */
    public Direction getDirection(float x1, float y1, float x2, float y2){
        double angle = getAngle(x1, y1, x2, y2);
        return Direction.get(angle);
    }

    /**
     *
     * Finds the angle between two points in the plane (x1,y1) and (x2, y2)
     * The angle is measured with 0/360 being the X-axis to the right, angles
     * increase counter clockwise.
     *
     * @param x1 the x position of the first point
     * @param y1 the y position of the first point
     * @param x2 the x position of the second point
     * @param y2 the y position of the second point
     * @return the angle between two points
     */
    public double getAngle(float x1, float y1, float x2, float y2) {

        double rad = Math.atan2(y1-y2,x2-x1) + Math.PI;
        return (rad*180/Math.PI + 180)%360;
    }

    public enum Direction{
        up,
        down,
        left,
        right;

        /**
         * Returns a direction given an angle.
         * Directions are defined as follows:
         *
         * Up: [45, 135]
         * Right: [0,45] and [315, 360]
         * Down: [225, 315]
         * Left: [135, 225]
         *
         * @param angle an angle from 0 to 360 - e
         * @return the direction of an angle
         */
        public static Direction get(double angle){
            if(inRange(angle, 45, 135)){
                return Direction.up;
            }
            else if(inRange(angle, 0,45) || inRange(angle, 315, 360)){
                return Direction.right;
            }
            else if(inRange(angle, 225, 315)){
                return Direction.down;
            }
            else{
                return Direction.left;
            }

        }

        /**
         * @param angle an angle
         * @param init the initial bound
         * @param end the final bound
         * @return returns true if the given angle is in the interval [init, end).
         */
        private static boolean inRange(double angle, float init, float end){
            return (angle >= init) && (angle < end);
        }
    }

}
