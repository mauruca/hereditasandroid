/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.adaptadores;

import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by mauricio on 01/07/16.
 */
class EtiquetaViewHolder {
    TextView text;
    TextView qt;
    ProgressBar sinal;

    boolean isValid()
    {
        return text != null && qt != null && sinal !=null;
    }
}
