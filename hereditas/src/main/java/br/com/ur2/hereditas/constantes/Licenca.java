/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.constantes;

/**
 * Created by mauricio on 26/05/16.
 */
public class Licenca {
    public static int tamanhoChave = 30;
    public static int tamanhoSerial = 8;
}
