/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.controlador;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import br.com.ur2.hereditas.modelo.Base;
import br.com.ur2.hereditas.modelo.Bem;

/**
 * Created by mauricio on 19/05/16.
 */
public class BemDadosControlador {

    private static BemDadosControlador instance = new BemDadosControlador();
    public static BemDadosControlador getInstance() { return instance; }

    // Dados em cache
    private ArrayList<Bem> bens = new ArrayList<Bem>();

    public ArrayList<Bem> obtemListaArray()
    {
        return bens;
    }

    public Integer contagem()
    {
        return bens.size();
    }

    private ArrayList<Bem> deserializador(String json) {
        if (json.trim().length() == 0)
            return null;
        Gson gson = new Gson();
        Type collectionType = new TypeToken<ArrayList<Bem>>() {}.getType();
        return gson.fromJson(json, collectionType);
    }
    
    public void atualizarListaServidor(String json)
    {
        if(json.trim().length() == 0)
            return;
        bens = deserializador(json);
        for (Bem b : bens) {
            if(b.rfid.trim().length() > 0)
                b.atribuiEstadoOK();
        }
    }

    public Bem buscaBemRFID(String rfid)
    {
        if(rfid == null)
            return null;
        rfid = rfid.trim().toUpperCase();
        if(rfid.length() == 0)
            return null;

        for (Bem bem: bens) {
            if(bem.rfid.equals(rfid))
                return bem;
        }
        return null;
    }

    public void alteraRfidBem(String pk,String rfid)
    {
        if(pk == null || rfid == null)
            return;
        pk = pk.trim();
        rfid = rfid.trim();
        if(pk.length() == 0 || rfid.length() == 0)
            return;

        for (Bem bem: bens) {
            if(bem.pk == pk) {
                bem.rfid = rfid;
                bem.atribuiEstadoSincronizar();
            }
        }
    }

    public ArrayList<Bem> obtemListaBensVazia()
    {
        ArrayList<Bem> retorno = new ArrayList<Bem>();
        for (Bem bem: bens) {
            if(bem.obtemEstado() == Base.EstadoRFID_Vazio)
                retorno.add(bem);
        }
        return retorno;
    }


    public ArrayList<Bem> obtemListaBensAtualizar()
    {
        ArrayList<Bem> retorno = new ArrayList<>();
        for (Bem bem: bens) {
            if(bem.obtemEstado() == Base.EstadoRFID_Sincronizar)
                retorno.add(bem);
        }
        return retorno;
    }
}
