/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.controlador;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import br.com.ur2.hereditas.modelo.Bem;
import br.com.ur2.hereditas.modelo.InventarioDataTransport;
import br.com.ur2.hereditas.modelo.Local;
import br.com.ur2.hereditas.modelo.Pessoa;
import br.com.ur2.dispositivo.modelo.Memoria;
import br.com.ur2.dispositivo.modelo.Tag;

/**
 * Created by mauricio on 28/05/16.
 */
public class InventarioDadosControlador {

    private static InventarioDadosControlador instance = new InventarioDadosControlador();
    public static InventarioDadosControlador getInstance() { return instance; }

    public ArrayList<Bem> listaBens = new ArrayList<Bem>();

    public Local localSelecionadoInventario;

    public ArrayList<InventarioDataTransport> inventarios = new ArrayList<InventarioDataTransport>();

    public void limparMemoriaLocais()
    {
        localSelecionadoInventario = null;
    }

    public void limparMemoriaBens()
    {
        listaBens.clear();
    }

    public void limpaMemoriaInventarios()
    {
        inventarios.clear();
    }

    public ArrayList<Local> processaLocaisTagsMemoria(Memoria memoria) {
        ArrayList<Local> retorno = new ArrayList<Local>();

        for (Tag tag : memoria.obtemListEtiquetas()) {
            Local lAux = LocalDadosControlador.getInstance().buscaLocalRFID(tag.obtemId());
            if (lAux == null)
                continue;
            retorno.add(lAux);
        }
        return retorno;
    }

    public ArrayList<Bem> processaBensTagsMemoria(Memoria memoria)
    {
        ArrayList<Bem> retorno = new ArrayList<Bem>();
        for (Tag tag : memoria.obtemListEtiquetas()) {
            Bem lAux = BemDadosControlador.getInstance().buscaBemRFID(tag.obtemId());
            if (lAux == null)
                continue;
            retorno.add(lAux);
        }
        listaBens = retorno;
        return retorno;
    }

    public boolean adicionaInventario()
    {
        if(localSelecionadoInventario == null || InventarioDadosControlador.getInstance().listaBens.size() == 0 || HereditasControlador.getInstance().obtemNomeUsuario().length() == 0)
            return false;
        Pessoa p = PessoaDadosControlador.getInstance().buscaPessoaPorNomeusuario(HereditasControlador.getInstance().obtemNomeUsuario());
        if(p == null)
            return false;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());
        currentDateandTime = currentDateandTime.replace(' ','T');
        for (Bem bem: listaBens) {
            inventarios.add(new InventarioDataTransport(currentDateandTime,bem.pk,localSelecionadoInventario.pk,p.user));
        }
        InventarioDadosControlador.getInstance().limparMemoriaBens();
        InventarioDadosControlador.getInstance().limparMemoriaLocais();
        return true;
    }
}
