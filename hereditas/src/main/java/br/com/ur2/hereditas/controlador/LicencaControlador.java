/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.controlador;

import java.security.InvalidParameterException;

import br.com.ur2.hereditas.constantes.Licenca;
import br.com.ur2.hereditas.tarefas.APILicencaAsyncTask;
import br.com.ur2.hereditas.*;
import br.com.ur2.util.Facilitador;
import br.com.ur2.util.Hash;

/**
 * Created by mauricio on 19/05/16.
 */
public class LicencaControlador {

    private static LicencaControlador instance = new LicencaControlador();
    public static LicencaControlador getInstance() { return instance; }

    private String chaveSerialId = "serial";

    private String serial = "";
    public boolean licencaVerificada = false;

    public void configurar()
    {
        if(serial.length() == 0)
            this.serial = Facilitador.obterPreferenciaString(chaveSerialId);
    }

    public boolean SerialValido()
    {
        return serial.trim().length() == Licenca.tamanhoSerial;
    }

    public void defineSerial(String serial)
    {
        if(serial == null)
            return;
        this.serial = serial.trim();
        if(serial.length() == 0)
        {
            serial = "";
        }
        if(serial.length() > 0 && serial.length() != Licenca.tamanhoSerial)
        {
            throw new InvalidParameterException(Facilitador.obtemString(R.string.serialInvalido));
        }
        Facilitador.definirPreferencia(chaveSerialId,serial);
    }

    public void removeSerial()
    {
        serial = "";
        Facilitador.removerPreferencia(chaveSerialId);
    }

    public String obtemSerial()
    {
        return this.serial;
    }

    public String obtemChave()
    {
        if(!SerialValido())
            return "";
        return Hash.DoSHA512(serial,Facilitador.obtemUID(),Licenca.tamanhoChave);
    }

    public void licenca(ResultadoAssincrono delegate)
    {
        APILicencaAsyncTask licTask = new APILicencaAsyncTask(delegate);
        licTask.execute(serial, obtemChave());
    }
}
