/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.controlador;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import br.com.ur2.hereditas.modelo.Base;
import br.com.ur2.hereditas.modelo.Local;

/**
 * Created by mauricio on 19/05/16.
 */
public class LocalDadosControlador {

    private static LocalDadosControlador instance = new LocalDadosControlador();
    public static LocalDadosControlador getInstance() { return instance; }

    // Dados em cache
    private ArrayList<Local> locais = new ArrayList<Local>();

    public Integer contagem()
    {
        return locais.size();
    }

    public ArrayList<Local> obtemListaArray()
    {
        return locais;
    }

    private ArrayList<Local> deserializador(String json) {
        if (json.trim().length() == 0)
            return null;
        Gson gson = new Gson();
        Type collectionType = new TypeToken<ArrayList<Local>>() {}.getType();
        return gson.fromJson(json, collectionType);
    }
    
    public void atualizarListaServidor(String json)
    {
        if(json.trim().length() == 0)
            return;
        locais = deserializador(json);
        for (Local b : locais) {
            if(b.rfid.trim().length() > 0)
                b.atribuiEstadoOK();
        }
    }

    public Local buscaLocalRFID(String rfid)
    {
        if(rfid == null)
            return null;
        rfid = rfid.trim();
        if(rfid.length() == 0)
            return null;

        for (Local local: locais) {
            if(local.rfid.equals(rfid))
                return local;
        }
        return null;
    }

    public void alteraRfidLocal(String pk,String rfid)
    {
        if(pk == null || rfid == null)
            return;
        pk = pk.trim();
        rfid = rfid.trim();
        if(pk.length() == 0 || rfid.length() == 0)
            return;

        for (Local local: locais) {
            if(local.pk == pk) {
                local.rfid = rfid;
                local.atribuiEstadoSincronizar();
            }
        }
    }

    public ArrayList<Local> obtemListaLocaisAtualizar()
    {
        ArrayList<Local> retorno = new ArrayList<Local>();
        for (Local local: locais) {
            if(local.obtemEstado() == Base.EstadoRFID_Sincronizar)
                retorno.add(local);
        }
        return retorno;
    }
}
