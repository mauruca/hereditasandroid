/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.controlador;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import br.com.ur2.hereditas.modelo.Pessoa;

/**
 * Created by mauricio on 19/05/16.
 */
public class PessoaDadosControlador {

    private static PessoaDadosControlador instance = new PessoaDadosControlador();
    public static PessoaDadosControlador getInstance() { return instance; }

    // Dados em cache
    private ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();

    public Integer Contagem()
    {
        return pessoas.size();
    }

    private ArrayList<Pessoa> Deserializador(String json) {
        if (json.trim().length() == 0)
            return null;
        Gson gson = new Gson();
        Type collectionType = new TypeToken<ArrayList<Pessoa>>() {}.getType();
        return gson.fromJson(json, collectionType);
    }
    
    public void AtualizarListaServidor(String json)
    {
        if(json.trim().length() == 0)
            return;
        pessoas = Deserializador(json);
        for (Pessoa b : pessoas) {
            if(b.rfid.trim().length() > 0)
                b.noServidor = true;
        }
    }

    public Pessoa buscaPessoaPorNomeusuario(String nomeusuario)
    {
        if(pessoas.size() == 0)
            return null;

        for (Pessoa pessoa: pessoas) {
            if(pessoa.login.equals(nomeusuario))
                return pessoa;
        }
        return null;
    }
}
