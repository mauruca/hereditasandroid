/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.modelo;

import com.google.gson.Gson;

/**
 * Created by mauricio on 18/05/16.
 */
public class RFIDDataTransport {
    public String rfid;

    public RFIDDataTransport(String r)
    {
        rfid = r;
    }

    public String Serializar()
    {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
