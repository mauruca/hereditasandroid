/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.modelo;

/**
 * Created by mauricio on 18/05/16.
 */
public class Token {

    private String _chave;

    public String chave() {
        return _chave;
    }

    public String nome() {
        return "Authorization";
    }

    public String apiChave() {
        return " Token " + chave();
    }

    public boolean valido() {
        return chave().length() == 40;
    }

    public Token(String p_chave) {
        this._chave = p_chave.trim();
    }

    @Override
    public String toString() {
        return "Authorization: Token " + chave();
    }
}
