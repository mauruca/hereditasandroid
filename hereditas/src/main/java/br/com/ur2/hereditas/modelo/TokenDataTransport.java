/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.modelo;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Created by mauricio on 18/05/16.
 */
public class TokenDataTransport {
    public String token = "";

    public static Token Deserializador(String json)
    {
        if (json.trim().length() == 0)
            return null;
        if(!json.toLowerCase().contains("token"))
            return null;
        Gson gson = new Gson();
        Type collectionType = new TypeToken<TokenDataTransport>() {}.getType();
        return new Token(((TokenDataTransport)gson.fromJson(json, collectionType)).token);
    }
}
