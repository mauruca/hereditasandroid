package br.com.ur2.hereditas.tarefas;

import android.os.AsyncTask;

import br.com.ur2.hereditas.ResultadoAssincrono;
import br.com.ur2.hereditas.controlador.AutenticacaoTokenControlador;
import br.com.ur2.hereditas.controlador.LicencaControlador;
import br.com.ur2.util.Erro;

/**
 * Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
 * Ver arquivo LICENSE em www.hereditas.net.br para detalhes.
 * Created by mauricio on 19/05/16.
 */
public class APILicencaAsyncTask extends AsyncTask<String, Void, Void> {
    public ResultadoAssincrono delegateResultado = null;
    String exMsg = "";

    public APILicencaAsyncTask(ResultadoAssincrono delegate)
    {
        delegateResultado = delegate;
    }

    @Override
    protected Void doInBackground(String... params) {
        try
        {
            LicencaControlador.getInstance().licencaVerificada = AutenticacaoTokenControlador.getInstance().validaTokenLicenca(params[0],params[1]);
        }
        catch (Exception e)
        {
            Erro.Registra(e);
            exMsg = e.getMessage();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(delegateResultado != null)
            delegateResultado.AoFinalizarTarefaAssincrona(exMsg);
    }
}
