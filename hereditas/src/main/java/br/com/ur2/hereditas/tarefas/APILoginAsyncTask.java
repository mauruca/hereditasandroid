package br.com.ur2.hereditas.tarefas;

import android.os.AsyncTask;

import br.com.ur2.hereditas.R;
import br.com.ur2.hereditas.ResultadoAssincrono;
import br.com.ur2.hereditas.controlador.AutenticacaoTokenControlador;
import br.com.ur2.util.Erro;
import br.com.ur2.util.Facilitador;

/**
 * Created by mauricio on 19/05/16.
 */
public class APILoginAsyncTask extends AsyncTask<String, Void, Void> {
    String exMsg = "";

    private ResultadoAssincrono delegateResultado = null;

    public APILoginAsyncTask(ResultadoAssincrono delegate) {
        delegateResultado = delegate;
    }

    @Override
    protected Void doInBackground(String... params) {
        try {
            AutenticacaoTokenControlador.getInstance().ValidaUsuario(params[0], params[1]);
        } catch (Exception e) {
            Erro.Registra(e);
            exMsg = e.getMessage();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (delegateResultado != null)
            delegateResultado.AoFinalizarTarefaAssincrona();
        if (exMsg.length() > 0) {
            Facilitador.informa(Facilitador.obtemString(R.string.autenticacaoFalha) + exMsg, true);
            return;
        }
        if (!AutenticacaoTokenControlador.getInstance().tokenValido()) {
            Facilitador.informa(Facilitador.obtemString(R.string.autenticacaoNExisteInvalida), false);
        }
    }
}
