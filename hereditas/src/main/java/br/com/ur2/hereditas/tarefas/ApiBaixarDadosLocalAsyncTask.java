package br.com.ur2.hereditas.tarefas;

import android.os.AsyncTask;

import br.com.ur2.hereditas.constantes.API;
import br.com.ur2.hereditas.controlador.LocalDadosControlador;
import br.com.ur2.restproxy.ClienteRESTProxy;
import br.com.ur2.util.Erro;
import br.com.ur2.util.Facilitador;

/**
 * Created by mauricio on 18/05/16.
 */
public class ApiBaixarDadosLocalAsyncTask extends AsyncTask<String, Integer, Integer>{

    String exMsg = "";

    @Override
    protected Integer doInBackground(String... params) {

        ClienteRESTProxy proxy = new ClienteRESTProxy();

        try {
            proxy.defineURL(params[0], API.apiPathLocal);
            proxy.defineToken(params[1],params[2]);
            LocalDadosControlador.getInstance().atualizarListaServidor(proxy.solicitacao(true));
            return LocalDadosControlador.getInstance().contagem();
        }
        catch (Exception e)
        {
            Erro.Registra(e);
            exMsg = e.getMessage();
            return 0;
        }
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        if(exMsg.length() > 0)
        {
            Facilitador.informa(exMsg,false);
            return;
        }
        Facilitador.informa("Foram baixados " + integer.toString() + " locais.",true);
    }
}
