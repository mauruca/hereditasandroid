package br.com.ur2.hereditas.tarefas;

import android.os.AsyncTask;

import javax.net.ssl.HttpsURLConnection;

import br.com.ur2.hereditas.constantes.API;
import br.com.ur2.hereditas.controlador.InventarioDadosControlador;
import br.com.ur2.hereditas.modelo.InventarioDataTransport;
import br.com.ur2.restproxy.ClienteRESTProxy;
import br.com.ur2.util.Erro;
import br.com.ur2.util.Facilitador;

/**
 * Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
 * Ver arquivo LICENSE em www.hereditas.net.br para detalhes.
 * Created by mauricio on 18/05/16.
 */
public class ApiEnviarInventariosAsyncTask extends AsyncTask<String, Void, Void> {

    String exMsg = "";

    int contagem = 0;
    int total = 0;

    @Override
    protected Void doInBackground(String... params) {
        ClienteRESTProxy proxy = new ClienteRESTProxy();

        try {

            proxy.method = ClienteRESTProxy.HttpVerb.POST;
            proxy.defineURL(params[0], API.apiPathInventario);
            proxy.defineToken(params[1],params[2]);

            total = InventarioDadosControlador.getInstance().inventarios.size();

            for (InventarioDataTransport idt: InventarioDadosControlador.getInstance().inventarios) {

                proxy.postData = idt.Serializar();
                proxy.solicitacao(true, HttpsURLConnection.HTTP_CREATED);
                contagem ++;
            }
        }
        catch (Exception e)
        {
            Erro.Registra(e);
            exMsg = e.getMessage();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        InventarioDadosControlador.getInstance().limpaMemoriaInventarios();
        Facilitador.informa(contagem + " de " + total +  " inventários enviados.",true);
        if(exMsg.length() > 0)
        {
            Facilitador.informa(exMsg,false);
        }
    }
}
