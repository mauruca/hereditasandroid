package br.com.ur2.hereditas.tarefas;

import android.os.AsyncTask;

import javax.net.ssl.HttpsURLConnection;

import br.com.ur2.hereditas.ResultadoAssincrono;
import br.com.ur2.hereditas.constantes.API;
import br.com.ur2.hereditas.controlador.LocalDadosControlador;
import br.com.ur2.hereditas.modelo.Local;
import br.com.ur2.hereditas.modelo.RFIDDataTransport;
import br.com.ur2.restproxy.ClienteRESTProxy;
import br.com.ur2.util.Erro;
import br.com.ur2.util.Facilitador;

/**
 * Created by mauricio on 18/05/16.
 */
public class ApiEnviarLocaisRFAsyncTask extends AsyncTask<String, Void, Void> {

    String exMsg = "";

    private ResultadoAssincrono delegateResultado = null;

    int contagem = 0;
    int total = 0;

    public ApiEnviarLocaisRFAsyncTask(ResultadoAssincrono delegate) {
        delegateResultado = delegate;
    }

    @Override
    protected Void doInBackground(String... params) {
        ClienteRESTProxy proxy = new ClienteRESTProxy();

        try {

            proxy.method = ClienteRESTProxy.HttpVerb.PUT;
            proxy.defineToken(params[1],params[2]);

            total = LocalDadosControlador.getInstance().obtemListaLocaisAtualizar().size();

            for (Local loc: LocalDadosControlador.getInstance().obtemListaLocaisAtualizar()) {

                proxy.defineURL(params[0], API.apiPathLocal + loc.pk + "/");

                RFIDDataTransport rfdt = new RFIDDataTransport(loc.rfid);
                proxy.postData = rfdt.Serializar();
                proxy.solicitacao(true, HttpsURLConnection.HTTP_OK);
                contagem ++;
            }
        }
        catch (Exception e)
        {
            Erro.Registra(e);
            exMsg = e.getMessage();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Facilitador.informa(contagem + " de " + total +  " locais enviados.",true);
        if(exMsg.length() > 0)
        {
            Facilitador.informa(exMsg,false);
        }
        if(contagem > 0 && delegateResultado != null)
            delegateResultado.AoFinalizarTarefaAssincrona();
    }
}
