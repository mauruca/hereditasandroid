package br.com.ur2.hereditas.tarefas;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Iterator;

import br.com.ur2.dispositivo.modelo.Tag;
import br.com.ur2.hereditas.controlador.BemDadosControlador;
import br.com.ur2.hereditas.modelo.Bem;

/**
 * Created by mauricio on 01/07/16.
 */
public class FiltraListaBensAsyncTask extends AsyncTask<CharSequence, ArrayList<Bem>, Void> {


    @Override
    protected Void doInBackground(CharSequence... params) {
        ArrayList<Bem> ret = new ArrayList<>(BemDadosControlador.getInstance().obtemListaArray());
        Iterator<Bem> it = ret.iterator();
        if(params[0].length() < 3)
            return null;
        int i = 0;
        while (it.hasNext()) {
            if (!it.next().descricao.contains(params[0])) {
                it.remove();
            }
            i++;
            if(i==100)
            {
                i=0;
                publishProgress(ret);
            }

        }
        return null;
    }
}
