package br.com.ur2.hereditas.tarefas;

import android.os.AsyncTask;

import java.util.ArrayList;

import br.com.ur2.hereditas.controlador.HereditasControlador;
import br.com.ur2.hereditas.controlador.InventarioDadosControlador;
import br.com.ur2.hereditas.modelo.Bem;
import br.com.ur2.util.Erro;

/**
 * Created by mauricio on 30/06/16.
 */
public class ProcessaBensEtiquetasMemoriaAsyncTask extends AsyncTask<Void, ArrayList<Bem>, Void> {

    @Override
    protected Void doInBackground(Void... params) {

        while (true)
        {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                Erro.Registra(e);
            }
            ArrayList<Bem> lista = InventarioDadosControlador.getInstance().processaBensTagsMemoria(HereditasControlador.getInstance().obtemDispositivo().obtemMemoria());
            if(lista.size() > 0)
                publishProgress(lista);
            // sai se cancelada
            if (isCancelled())
                break;
        }

        return null;
    }
}
