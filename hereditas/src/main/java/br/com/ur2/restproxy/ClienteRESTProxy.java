/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.restproxy;

/**
 * Created by mauricio on 18/05/16.
 * https://developer.android.com/samples/NetworkConnect/src/com.example.android.networkconnect/MainActivity.html#l142
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import javax.net.ssl.HttpsURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

import br.com.ur2.util.Erro;

public class ClienteRESTProxy {

    public static final String JSON = "application/json";
    public static final String XML = "text/xml";

    public static final int readTimeout = 15000;  /* milliseconds */
    public static final int connectTimeout = 3000;  /* milliseconds */

    public enum HttpVerb
    {
        GET,
        POST,
        PUT,
        DELETE;
    }

    public enum ContentType
    {
        JSON,
        XML
    }

    public ContentType accept = ContentType.JSON;
    public ContentType contentType = ContentType.JSON;
    public HttpVerb method = HttpVerb.GET;
    public String postData = "";

    private URL url;
    private String tokenKey = "";
    private String tokenValue = "";

    public void defineURL(String pEndereco, String pCaminho) throws MalformedURLException
    {
        if(pEndereco == null) { pEndereco = ""; }
        if(pCaminho == null) { pCaminho = ""; }
        url = new URL(pEndereco + pCaminho);
    }

    public void defineToken(String key, String value)
    {
        if(key == null) { key = ""; }
        if(value == null) { value = ""; }
        tokenKey = key.trim();
        tokenValue = value.trim();
    }

    public ClienteRESTProxy() {
    }

    public ClienteRESTProxy(String pEndereco, String pCaminho) throws MalformedURLException
    {
        defineURL(pEndereco,pCaminho);
        method = HttpVerb.POST;
    }

    public boolean temRede(Context cont) {
        ConnectivityManager connMgr = (ConnectivityManager)
                cont.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private boolean vaiEnviarDados()
    {
        return (postData != null && postData.trim().length() != 0) && (method == HttpVerb.POST || method == HttpVerb.PUT);
    }

    private String obtemDescricaoTipoConteudo(ContentType tipoConteudo)
    {
        if(tipoConteudo == null)
            return JSON;

        switch (tipoConteudo)
        {
            case JSON:
                return JSON;
            case XML:
                return XML;
            default:
                return JSON;
        }
    }

    private String obtemDescricaoVerboHTTP(HttpVerb verboHTTP)
    {
        if(verboHTTP == null)
            return verboHTTP.GET.toString();

        switch (verboHTTP)
        {
            case DELETE:
                return verboHTTP.toString();
            case GET:
                return verboHTTP.toString();
            case POST:
                return verboHTTP.toString();
            case PUT:
                return verboHTTP.toString();
            default:
                return verboHTTP.GET.toString();
        }
    }

    public String solicitacao(boolean enviaToken) throws IOException, InvalidParameterException {
        return solicitacao(enviaToken,HttpsURLConnection.HTTP_OK);
    }

    public String solicitacao(boolean enviaToken, Integer codigoEstadoHTTPEsperado) throws IOException, InvalidParameterException {
        // Valida assertivas de entrada
        if(enviaToken && (tokenKey.length() == 0 || tokenValue.length() == 0))
        {
            throw new InvalidParameterException("Token solicitado não foi informado.");
        }
        if(codigoEstadoHTTPEsperado == null) codigoEstadoHTTPEsperado = HttpsURLConnection.HTTP_OK;
        if(url == null)
        {
            throw new InvalidParameterException("URL não informada.");
        }

        // Converte o método pois não existem dados para serem enviados
        if(!vaiEnviarDados() && (method == HttpVerb.POST || method == HttpVerb.PUT))
            method = HttpVerb.GET;

        String retorno = "";

        if(url.getProtocol().toLowerCase().equals("https"))
            retorno = solicitacao((HttpsURLConnection) url.openConnection(),enviaToken,codigoEstadoHTTPEsperado);
        else
            retorno = solicitacao((HttpURLConnection) url.openConnection(),enviaToken,codigoEstadoHTTPEsperado);

        return retorno;
    }

    private String solicitacao(HttpsURLConnection connS, boolean enviaToken, Integer codigoEstadoHTTPEsperado) throws IOException, InvalidParameterException
    {
        String retorno = "";

        try {

            connS.addRequestProperty("Accept",obtemDescricaoTipoConteudo(accept));
            connS.addRequestProperty("Content-type",obtemDescricaoTipoConteudo(contentType));
            connS.setDoInput(true);
            connS.setRequestMethod(obtemDescricaoVerboHTTP(method));

            connS.setReadTimeout(readTimeout);
            connS.setConnectTimeout(connectTimeout);

            if(enviaToken)
            {
                connS.addRequestProperty(tokenKey,tokenValue);
            }

            // Insere dados se for post ou put
            if(vaiEnviarDados())
            {
                connS.setDoOutput(true);
                DataOutputStream out = new DataOutputStream(connS.getOutputStream());
                out.write(postData.getBytes("UTF-8"));
                out.flush();
                out.close();
            }
            else
                connS.connect();

            // Valida resposta
            if(connS.getResponseCode() != codigoEstadoHTTPEsperado)
            {
                String message = String.format("Request failed. Received HTTPS %1$s >> " + connS.getResponseMessage(), connS.getResponseCode());
                throw new IOException(message);
            }

            return readIt(connS.getInputStream());
        }
        catch (Exception e)
        {
            Erro.Registra(e);
            throw e;
        }
        finally {
                if(connS != null)
                connS.disconnect();
        }
    }
    private String solicitacao(HttpURLConnection conn, boolean enviaToken, Integer codigoEstadoHTTPEsperado) throws IOException, InvalidParameterException
    {
        String retorno = "";

        try {

            conn.addRequestProperty("Accept",obtemDescricaoTipoConteudo(accept));
            conn.addRequestProperty("Content-type",obtemDescricaoTipoConteudo(contentType));
            conn.setDoInput(true);
            conn.setRequestMethod(obtemDescricaoVerboHTTP(method));

            conn.setReadTimeout(readTimeout);
            conn.setConnectTimeout(connectTimeout);

            if(enviaToken)
            {
                conn.addRequestProperty(tokenKey,tokenValue);
            }

            // Insere dados se for post ou put
            if(vaiEnviarDados())
            {
                conn.setDoOutput(true);
                DataOutputStream out = new DataOutputStream(conn.getOutputStream());
                out.write(postData.getBytes("UTF-8"));
                out.flush();
                out.close();
            }
            else
                conn.connect();

            // Valida resposta
            if(conn.getResponseCode() != codigoEstadoHTTPEsperado)
            {
                String message = String.format("Request failed. Received HTTP %1$s >> " + conn.getResponseMessage(), conn.getResponseCode());
                throw new IOException(message);
            }

            retorno = readIt(conn.getInputStream());
            return retorno;
        }
        catch (Exception e)
        {
            Erro.Registra(e);
            return retorno;
        }
        finally {
            if(conn != null)
                conn.disconnect();
        }
    }

    // Reads an InputStream and converts it to a String.
    private String readIt(InputStream stream) throws IOException, UnsupportedEncodingException {

        BufferedReader rd = new BufferedReader(new InputStreamReader(stream), 4096);
        String line;
        StringBuilder sb =  new StringBuilder();
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        rd.close();
        return sb.toString();
    }

    public String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException{
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }
}
