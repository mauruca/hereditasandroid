/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.util;

/**
 * Created by mauricio on 18/05/16.
 */
public class Erro {
    public static void Registra(Exception e)
    {
        uLog.Registra("Erro",e.getClass().getCanonicalName(),e.getMessage(),1);
    }
}
