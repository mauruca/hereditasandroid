/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.widget.Toast;

/**
 * Created by mauricio on 04/06/16.
 */
public class Facilitador {

    public static String NOME_CONFIGURACAO = "Configuracoes";

    private static Context cx;

    public static void definirContexto(Context c)
    {
        cx = c;
    }

    private static void valida()
    {
        if(NOME_CONFIGURACAO == null || NOME_CONFIGURACAO.trim().length() == 0)
        {
            throw new IllegalArgumentException("Variável global NOME_CONFIGURACAO não definida.");
        }
    }

    public static String obtemUID()
    {
        return Settings.Secure.getString(cx.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String obtemString(int id)
    {
        return cx.getString(id);
    }

    public static void definirPreferencia(String chave, String valor)
    {
        if(chave == null || chave.trim().length() == 0 || valor == null || valor.trim().length() == 0)
            return;
        valida();
        SharedPreferences.Editor settings = cx.getSharedPreferences(NOME_CONFIGURACAO, Context.MODE_PRIVATE).edit();
        settings.putString(chave,valor);
        settings.apply();
    }

    public static void definirPreferencia(String chave, boolean valor)
    {
        if(chave == null || chave.trim().length() == 0)
            return;
        valida();
        SharedPreferences.Editor settings = cx.getSharedPreferences(NOME_CONFIGURACAO, Context.MODE_PRIVATE).edit();
        settings.putBoolean(chave,valor);
        settings.apply();
    }

    public static String obterPreferenciaString(String chave)
    {
        if(chave == null || chave.trim().length() == 0)
            return "";
        valida();
        SharedPreferences settings = cx.getSharedPreferences(NOME_CONFIGURACAO, Context.MODE_PRIVATE);
        return settings.getString(chave,"");
    }

    public static boolean obterPreferenciaBoolean(String chave)
    {
        if(chave == null || chave.trim().length() == 0)
            return false;
        valida();
        SharedPreferences settings = cx.getSharedPreferences(NOME_CONFIGURACAO, Context.MODE_PRIVATE);
        return settings.getBoolean(chave,false);
    }

    public static void removerPreferencia(String chave)
    {
        if(chave == null || chave.trim().length() == 0)
            return;
        valida();
        SharedPreferences.Editor settings = cx.getSharedPreferences(NOME_CONFIGURACAO, Context.MODE_PRIVATE).edit();
        settings.remove(chave);
        settings.apply();
    }

    public static void informa(String msg, boolean rapida)
    {
        if(msg == null || msg.trim().length() == 0)
            return;
        if(rapida)
        {
            Toast.makeText(cx, msg.trim(), Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(cx, msg.trim(), Toast.LENGTH_LONG).show();
    }
}
