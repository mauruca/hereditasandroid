/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.util;

import android.util.Log;

/**
 * Created by mauricio on 18/05/16.
 */
public class uLog {
    public static boolean debug = false;
    public static int nivelRegistro = 2;
    public static void Registra(String TAG, String mensagem)
    {
        Registra(TAG,"",mensagem);
    }
    public static void Registra(String TAG, String metodo, String mensagem)
    {
        Registra(TAG,metodo, mensagem,3);
    }
    public static void Registra(String TAG, String metodo, String mensagem, int nivel)
    {
        if(!debug)
            return;
        if(nivelRegistro >= nivel)
            Log.d(TAG,metodo + " : " + mensagem);
    }
}
