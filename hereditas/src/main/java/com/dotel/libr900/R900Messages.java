package com.dotel.libr900;

import android.bluetooth.BluetoothClass;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 * Created by mauricio on 15/06/16.
 */
public class R900Messages {
    // ---
    public static final int MSG_BT_DATA_RECV = 10;
    public static final int MSG_BT_DATA_SENT = 11;
    public static final int MSG_BT_DATA_EXCEPTION = 12;

    public static final int MSG_BT_CONNECTED = 20;
    public static final int MSG_BT_DISCONNECTED = 21;
    public static final int MSG_BT_FAILED_CONNECTED = 22;

    private static boolean validMessageConstant(int messageConstant)
    {
        return messageConstant == MSG_BT_DATA_RECV || messageConstant == MSG_BT_DATA_SENT || messageConstant == MSG_BT_DATA_EXCEPTION ||
                messageConstant == MSG_BT_CONNECTED || messageConstant == MSG_BT_DISCONNECTED || messageConstant == MSG_BT_FAILED_CONNECTED;
    }

    public static final int MSG_QUIT = 9999;

    public static final int MSG_ENABLE_DISCONNECT = 999912;
    public static final int MSG_DISABLE_DISCONNECT = 999913;
    public static final int MSG_SHOW_TOAST = 999920;

    public static final int MSG_AUTO_REFRESH_DEVICE = 99991;
    public static final int MSG_REFRESH_LIST_DEVICE = 999921;
    public static final int MSG_REFRESH_LIST_TAG = 999922;

    public static final int MSG_SOUND_RX = 999940;
    public static final int MSG_SOUND_RX_HALF = 999941;

    public static final int MSG_AUTO_LINK = 9999100;
    public static final int MSG_ENABLE_LINK_CTRL = 999910;
    public static final int MSG_DISABLE_LINK_CTRL = 999911;
    public static final int MSG_LINK_ON = 999930;
    public static final int MSG_LINK_OFF = 999931;

    private static final String msgKey = "r900msg";

    public static void notify(Handler h, int messageConstant)
    {
        notify(h, messageConstant, "");
    }

    public static void notify(Handler h, int messageConstant, String message)
    {
        if(h == null || !validMessageConstant(messageConstant))
            return;
        if(message.trim().length() == 0)
        {
            h.sendEmptyMessage(messageConstant);
            return;
        }

        Bundle b = new Bundle();
        b.putString(msgKey,message.trim());

        Message m = new Message();
        m.what = messageConstant;
        m.setData(b);
        h.sendMessage(m);
    }

    public static void notify(Handler h, int messageConstant, byte[] message)
    {
        if(h == null || !validMessageConstant(messageConstant))
            return;
        if(message.length == 0)
        {
            h.sendEmptyMessage(messageConstant);
            return;
        }

        Bundle b = new Bundle();
        b.putByteArray(msgKey,message);

        Message m = new Message();
        m.what = messageConstant;
        m.setData(b);
        h.sendMessage(m);
    }

    public static String getStringMessageData(Message m)
    {
        return m.getData().getString(msgKey);
    }

    public static byte[] getByteArrayMessageData(Message m)
    {
        return m.getData().getByteArray(msgKey);
    }

}
