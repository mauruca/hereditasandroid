package br.com.ur2.dispositivo;

import android.os.Handler;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.runners.statements.ExpectException;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;
import java.util.EmptyStackException;

import br.com.ur2.dispositivo.modelo.Dispositivo;
import br.com.ur2.dispositivo.modelo.Memoria;

/**
 * Created by Mauricio Pinheiro on 19/09/16.
 */
public class ZXingTest {

    @Test
    public void obtemMemoriaValidaDispositivoBaseNovo()
    {
        Memoria mExpected = new Memoria();
        Memoria mActual = SenhorDosDispositivos.meEmpresta(Dispositivo.TipoDispositivo.ZXing).obtemMemoria();
        assertEquals(mExpected,mActual);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void configuraHandlerDispositivoValido() {
        Dispositivo d = SenhorDosDispositivos.meEmpresta(Dispositivo.TipoDispositivo.ZXing);
        d.configuraHandlerDispositivo(new Handler());
    }

    @Test
    public void configuraHandlerDadosValido() {
        Dispositivo d = SenhorDosDispositivos.meEmpresta(Dispositivo.TipoDispositivo.ZXing);
        d.configuraHandlerDados(new Handler());
    }
}
