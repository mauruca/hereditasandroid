package br.com.ur2.dispositivo.modelo;

/**
 * Created by mauricio on 17/06/16.
 */
import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class MemoriaTest {

    @Rule
    public ExpectedException ex = ExpectedException.none();

    @Test
    public void novaMemoriaSemEtiquetas()
    {
        Memoria m = new Memoria();
        assertEquals(0,m.totalEtiquetas());
    }

    @Test
    public void novaMemoriaSemUltimaEtiquetas()
    {
        Memoria m = new Memoria();
        assertEquals(null,m.obtemUltimaEtiqueta());
    }

    @Test
    public void adicionaSemSinalNula()
    {
        Memoria m = new Memoria();
        m.adicionaEtiqueta(null);
        assertEquals(null,m.obtemUltimaEtiqueta());
    }

    @Test
    public void adicionaSemSinalVazia()
    {
        Memoria m = new Memoria();
        m.adicionaEtiqueta("");
        assertEquals(null,m.obtemUltimaEtiqueta());
    }

    @Test
    public void adicionaSemSinalComEspacos()
    {
        Memoria m = new Memoria();
        m.adicionaEtiqueta("          ");
        assertEquals(null,m.obtemUltimaEtiqueta());
    }

    @Test
    public void adicionaSemSinalUltima()
    {
        String idEtiquetaEsperada = "123";
        Memoria m = new Memoria();
        m.adicionaEtiqueta("123");
        assertEquals(idEtiquetaEsperada,m.obtemUltimaEtiqueta().obtemId());
    }

    @Test
    public void adicionaComSinalIdNula()
    {
        Memoria m = new Memoria();
        m.adicionaEtiqueta(null,0);
        assertEquals(null,m.obtemUltimaEtiqueta());
    }

    @Test
    public void adicionaComSinalIdVazia()
    {
        Memoria m = new Memoria();
        m.adicionaEtiqueta("",0);
        assertEquals(null,m.obtemUltimaEtiqueta());
    }

    @Test
    public void adicionaComSinalBusca()
    {
        String idEtiquetaEsperada = "123";
        float sinalEsperado = (float) -34.44;
        Memoria m = new Memoria();
        m.adicionaEtiqueta("123",(float) -34.44);

        assertEquals(idEtiquetaEsperada,m.buscaEtiqueta(idEtiquetaEsperada).obtemId());
        assertEquals(sinalEsperado,m.buscaEtiqueta(idEtiquetaEsperada).obtemSinal(),0);
    }

    @Test
    public void ultimaEtiquetaDeDuas ()
    {
        String idEtiquetaEsperada = "234";
        Memoria m = new Memoria();
        m.adicionaEtiqueta("123");
        m.adicionaEtiqueta("234");
        assertEquals(idEtiquetaEsperada,m.obtemUltimaEtiqueta().obtemId());
    }

    @Test
    public void quantidadeEtiquetas1()
    {
        int qtdEsperada = 1;
        Memoria m = new Memoria();
        m.adicionaEtiqueta("123");
        assertEquals(qtdEsperada,m.totalEtiquetas());
    }

    @Test
    public void quantidadeEtiquetas3()
    {
        int qtdEsperada = 3;
        Memoria m = new Memoria();
        m.adicionaEtiqueta("123");
        m.adicionaEtiqueta("234");
        m.adicionaEtiqueta("345");
        assertEquals(qtdEsperada,m.totalEtiquetas());
    }

    @Test
    public void buscaEtiqueta()
    {
        String idEtiquetaEsperada = "234";
        Memoria m = new Memoria();
        m.adicionaEtiqueta("123");
        m.adicionaEtiqueta("234",(float) -34.44);
        m.adicionaEtiqueta("345");
        assertEquals(idEtiquetaEsperada,m.buscaEtiqueta(idEtiquetaEsperada).obtemId());
    }

    @Test
    public void limpaMemoriaUltimaNula()
    {
        Memoria m = new Memoria();
        m.adicionaEtiqueta("123");
        m.adicionaEtiqueta("234",(float) -34.44);
        m.adicionaEtiqueta("345");
        m.limpa();
        assertEquals(null,m.obtemUltimaEtiqueta());
    }

    @Test
    public void limpaMemoriaQtdZero()
    {
        Memoria m = new Memoria();
        m.adicionaEtiqueta("123");
        m.adicionaEtiqueta("234",(float) -34.44);
        m.adicionaEtiqueta("345");
        m.limpa();
        assertEquals(0,m.totalEtiquetas());
    }
}
