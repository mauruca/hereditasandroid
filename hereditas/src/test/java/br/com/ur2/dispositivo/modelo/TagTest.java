package br.com.ur2.dispositivo.modelo;

/**
 * Created by mauricio on 17/06/16.
 */

import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class TagTest {
    @Rule
    public ExpectedException ex = ExpectedException.none();

    @Test
    public void ConstrutorVazio()
    {
        Tag t = new Tag();
        assertEquals("",t.obtemId());
        assertEquals(0,t.obtemQuantidade());
        assertEquals(0,t.obtemSinal(),0);
        assertFalse(t.valido());
    }

    @Test
    public void ConstrutorId()
    {
        String idEsperado = "6756RTHRJHRJRYJ";
        Tag t = new Tag(idEsperado);
        assertEquals(idEsperado,t.obtemId());
        assertEquals(0,t.obtemQuantidade());
        assertEquals(0,t.obtemSinal(),0);
        assertTrue(t.valido());
    }

    @Test
    public void defineIdNulo()
    {
        String idEsperado = "";
        Tag t = new Tag();
        t.defineId(null);
        assertEquals(idEsperado,t.obtemId());
        assertFalse(t.valido());
    }

    @Test
    public void defineIdVazio()
    {
        String idEsperado = "";
        Tag t = new Tag();
        t.defineId(idEsperado);
        assertEquals(idEsperado,t.obtemId());
        assertFalse(t.valido());
    }

    @Test
    public void defineId()
    {
        String idEsperado = "6756RTHRJHRJRYJ";
        Tag t = new Tag();
        t.defineId(idEsperado);
        assertEquals(idEsperado,t.obtemId());
        assertTrue(t.valido());
    }

    @Test
    public void defineQtdMaisum()
    {
        int qtdEsperada = 1;
        Tag t = new Tag();
        t.maisUmRegistro();
        assertEquals(qtdEsperada,t.obtemQuantidade());
    }

    @Test
    public void defineQtdMais3()
    {
        int qtdEsperada = 3;
        Tag t = new Tag();
        t.maisUmRegistro();
        t.maisUmRegistro();
        t.maisUmRegistro();
        assertEquals(qtdEsperada,t.obtemQuantidade());
    }

    @Test
    public void defineRSSIZero()
    {
        float idEsperado = 0;
        Tag t = new Tag();
        t.defineSinal(idEsperado);
        assertEquals(idEsperado,t.obtemSinal(),0);
    }

    @Test
    public void defineRSSINegativo()
    {
        float idEsperado = (float) -99.99;
        Tag t = new Tag();
        t.defineSinal(idEsperado);
        assertEquals(idEsperado,t.obtemSinal(),0);
    }

    @Test
    public void obtemForcaSinal0_sinal99ponto99()
    {
        int forcaEsperada = 0;
        Tag t = new Tag();
        t.defineSinal((float) -99.99);
        assertEquals(forcaEsperada,t.forcaSinal());
    }

    @Test
    public void obtemForcaSinal0_sinal80()
    {
        int forcaEsperada = 0;
        Tag t = new Tag();
        t.defineSinal((float) -80);
        assertEquals(forcaEsperada,t.forcaSinal());
    }

    @Test
    public void obtemForcaSinal5_sinal79ponto99()
    {
        int forcaEsperada = 5;
        Tag t = new Tag();
        t.defineSinal((float) -79.99);
        assertEquals(forcaEsperada,t.forcaSinal());
    }

    @Test
    public void obtemForcaSinal20_sinal70()
    {
        int forcaEsperada = 20;
        Tag t = new Tag();
        t.defineSinal((float) -70);
        assertEquals(forcaEsperada,t.forcaSinal());
    }

    @Test
    public void obtemForcaSinal25_sinal69ponto99()
    {
        int forcaEsperada = 25;
        Tag t = new Tag();
        t.defineSinal((float) -69.99);
        assertEquals(forcaEsperada,t.forcaSinal());
    }

    @Test
    public void obtemForcaSinal45_sinal60()
    {
        int forcaEsperada = 45;
        Tag t = new Tag();
        t.defineSinal((float) -60);
        assertEquals(forcaEsperada,t.forcaSinal());
    }

    @Test
    public void obtemForcaSinal50_sinal59ponto99()
    {
        int forcaEsperada = 50;
        Tag t = new Tag();
        t.defineSinal((float) -59.99);
        assertEquals(forcaEsperada,t.forcaSinal());
    }

    @Test
    public void obtemForcaSinal70_sinal50()
    {
        int forcaEsperada = 70;
        Tag t = new Tag();
        t.defineSinal((float) -50);
        assertEquals(forcaEsperada,t.forcaSinal());
    }

    @Test
    public void obtemForcaSinal75_sinal49ponto99()
    {
        int forcaEsperada = 75;
        Tag t = new Tag();
        t.defineSinal((float) -49.99);
        assertEquals(forcaEsperada,t.forcaSinal());
    }

    @Test
    public void obtemForcaSinal95_sinal40()
    {
        int forcaEsperada = 95;
        Tag t = new Tag();
        t.defineSinal((float) -40);
        assertEquals(forcaEsperada,t.forcaSinal());
    }

    @Test
    public void obtemForcaSinal100_sinal39ponto99()
    {
        int forcaEsperada = 100;
        Tag t = new Tag();
        t.defineSinal((float) -39.99);
        assertEquals(forcaEsperada,t.forcaSinal());
    }

    @Test
    public void obtemForcaSinal100_sinal1()
    {
        int forcaEsperada = 100;
        Tag t = new Tag();
        t.defineSinal((float) -1);
        assertEquals(forcaEsperada,t.forcaSinal());
    }

    @Test
    public void EstaValido()
    {
        Tag t = new Tag("rgergeg");
        assertTrue(t.valido());
    }

    @Test
    public void EstaInValido()
    {
        Tag t = new Tag();
        assertFalse(t.valido());
    }

    @Test
    public void IguaisNovos()
    {
        Tag t1 = new Tag();
        Tag t2 = new Tag();
        assertTrue(t1.equals(t2));
    }

    @Test
    public void IguaisComId()
    {
        Tag t1 = new Tag("erethth");
        Tag t2 = new Tag("erethth");
        assertTrue(t1.equals(t2));
    }

    @Test
    public void Diferentes()
    {
        Tag t1 = new Tag("erethth1");
        Tag t2 = new Tag("erethth2");
        assertFalse(t1.equals(t2));
    }

}
